import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int answer = 1;
        int counter = 1;

        System.out.println("Input an integer whose factorial will be computed");
        try {
            int num = in.nextInt();
            if (num < 0) {
                System.out.println("Please enter a positive integer!");
            } else {
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }
                System.out.println("The factorial of " + num + " is " + answer);

                answer = 1;
                for (counter = 1; counter <= num; counter++) {
                    answer *= counter;
                }
                System.out.println("The factorial of " + num + " is " + answer);
            }
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
    }
}